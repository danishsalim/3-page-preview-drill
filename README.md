
## Overview

### The challenge

Users should be able to:

- View the optimal layout depending on their device's screen size
- See hover states for interactive elements

### screenshot
![alt text](image.png)
![alt text](image-1.png)


### Links

- Mobile View Solution URL: https://columnspreviewmoblie.netlify.app/
- Desktop View URL: https://cardspreview.netlify.app/

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- Mobile-first workflow







